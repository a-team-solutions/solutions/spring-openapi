package api.customvalidations;

public enum DateTimeType {
    DateTime,
    Date,
    Time
}
