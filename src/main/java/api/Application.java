package api;

import java.util.HashMap;

import javax.annotation.PostConstruct;
import javax.money.MonetaryAmount;

import org.javamoney.moneta.Money;
import org.springdoc.core.SpringDocUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.zalando.jackson.datatype.money.MoneyModule;

import com.fasterxml.jackson.databind.ObjectMapper;

import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.info.License;

@SpringBootApplication
@ComponentScan(basePackages = { "api" })
public class Application {

    public static void main(String[] args) {
        // SpringApplication.run(Application.class, args);

        HashMap<String, Object> props = new HashMap<>();
        // props.put("server.port", 8090); // or use application.properties

        new SpringApplicationBuilder()
            .sources(Application.class)
            .properties(props)
            .run(args);
    }

    @Autowired
    ObjectMapper objectMapper;

    @PostConstruct
    void init() {
        SpringDocUtils.getConfig().replaceWithClass(MonetaryAmount.class,
                org.springdoc.core.converters.MonetaryAmount.class);
        CustomConverter.add(objectMapper);
    }

    @Bean
    public MoneyModule moneyModule() {
        return new MoneyModule().withMonetaryAmount(Money::of);
    }

    @Bean
    public OpenAPI customOpenAPI(@Value("${application-description}") String appDesciption,
            @Value("${application-version}") String appVersion) {
        return new OpenAPI().info(new Info().title("Sample application API").version(appVersion)
                .description(appDesciption).termsOfService("http://swagger.io/terms/")
                .license(new License().name("Apache 2.0").url("http://springdoc.org")));
    }

}